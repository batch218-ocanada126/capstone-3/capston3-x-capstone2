import './App.css';

import { useState, useEffect } from 'react';

import {UserProvider} from './UserContext';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

//Container
import {Container} from 'react-bootstrap';

//Routes for AppNavbar
import AppNavbar from './components/AppNavbar';
//Routes for components
import ProductView from './components/ProductView';
import TopLogo from './components/TopLogo';
import CreateProductView from './components/CreateProductView';
import UpdateProductView from './components/UpdateProductView';

//Routes for pages
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminDashboard from './pages/AdminDashboard';



function App() {

	// State hook for the user state 
	// global scope 
	// This will be used to store the user information and will be used for validating if a user is logged in on app or not
	// const [user, setUser] = useState({email: localStorage.getItem('email')});
	const [user, setUser] = useState({
	  id: null,
	  isAdmin: null

	});

	//Function for clearing localStorage on logout
	const unsetUser = () =>{
	  localStorage.clear();

	}

	// used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout.
	useEffect(() => {
	    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	        headers: {
	            Authorization: `Bearer ${localStorage.getItem('token')}`
	        }
	    })
	    .then(res => res.json())
	    .then(data => {
	        // user is logged in
	        if(typeof data._id !== "undefined") {

	            setUser({
	                id: data._id,
	                isAdmin: data.isAdmin
	            })
	        } 
	        // User is not logged in or logout
	        else { 
	            setUser({
	                id: null,
	                isAdmin: null
	            })
	        }

	    })
	}, []);


	return(
		<>

	 <UserProvider value={{user, setUser, unsetUser}}>
		<Router>
		
      <AppNavbar />
      	
        <Routes>

          <Route path="/" element={<Home />} />
          <Route path="/products" element={<Products />} />
          <Route path="/create-product" element={<CreateProductView />} />
          <Route path="/update-product/:productId" element={<UpdateProductView />} />
          <Route path="/products/:productId" element={<ProductView />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/adminDashboard" element={<AdminDashboard />} />
          <Route path="*" element={<Error />} /> {/* "*" - wildcard character that will match any path that was defined in routes */}
        </Routes>
    
    
    </Router>
    </UserProvider>
		</>
	)
}

export default App;