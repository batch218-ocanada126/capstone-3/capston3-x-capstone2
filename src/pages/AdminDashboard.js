
import { useState, useContext} from 'react';

import { useNavigate, Link} from 'react-router-dom';

import UserContext from '../UserContext';

import {  Button, Table } from 'react-bootstrap';







import Swal from 'sweetalert2';





	export default function AdminRole() {
		

	 const [selectedProductId, setSelectedProductId] = useState(null);



	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	
	const [products, setProducts] = useState([]);

	
	
	

	const [productData, setProductData] = useState({});

	
        const getAllProducts = (e) => {
            e.preventDefault();
            // Check if user is admin
           
            	fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`, {
            	
            	})
            	.then(res => res.json())
            	.then(data => {
           			 console.log(data);
            		setProducts(data);
           		 })
     
           
          };



          	const updateProduct = (productId) => {
          	    //Fetch the product data from the API
          	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
          	      .then(res => res.json())
          	      .then(data => {
          	        // set the product data in the state
          	        setProductData(data);
          	        setSelectedProductId(productId);
          	        // navigate to the update product view
          	        navigate(`/update-product/${productId}`);
          	      });
          	  }


          /* useEffect(() => {

                          fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)

                            .then(res => res.json())
                            .then(data => {
                              setProducts(data)
                              console.log(data)
                                })
                            .catch(err => console.log(err));
                        }, []);*/

             const handleDeactivateProduct = (productId) => {
               setSelectedProductId(productId);
  
               fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
                 method: 'PUT',
                 headers: {
                   'Content-Type': 'application/json',
                   Authorization: `Bearer ${localStorage.getItem('token')}`,
                 },
               })
                 .then(res => res.json())
                 .then(data => {
                   if (data !== null) {
                     Swal.fire({
                       title: "Product deactivated successfully",
                       icon: "success",
                       text: "The product has been deactivated.",
                     });
                    
                     setProducts(prevProducts => prevProducts.map(product => {
                       if (product._id === productId) {
                         product.isActive = false;
                       }
                       return product;
                     }));
                   } else {
                     Swal.fire({
                       title: "Error deactivating product",
                       icon: "error",
                       text: "There was an error deactivating the product. Please try again.",
                     });
                   }
                 })
               }   

             const handleReactivateProduct = (productId) => {
             setSelectedProductId(productId);

               fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
                          method: 'PUT',
                          headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`,
                          },
                          body: JSON.stringify({isActive: true})
                        })
                          .then(res => res.json())
                          .then(data => {
                            if (data !== null) {
                              Swal.fire({
                                title: "Product reactivated successfully",
                                icon: "success",
                                text: "The product has been reactivated.",
                              });
                             
                              setProducts(prevProducts => prevProducts.map(product => {
                                if (product._id === productId) {
                                  product.isActive = true;
                                }
                                return product;
                              }));
                            } else {
                              Swal.fire({
                                title: "Error reactivating product",
                                icon: "error",
                                text: "There was an error reactivating the product. Please try again.",
                              });
                            }
                          })
                        }  

        




	return (

<div id="bg-AD">
<div id="dashboardBg">	
  <div className="container-fluid text-center">
  	
		
	<h1 className="text-dark" id="admin">Admin Dashboard</h1>
	
   </div>
   	<div className="container-fluid text-center">	
	  <Link to="/create-product">
        <Button variant="outline-primary">Create Product</Button>
      </Link>
		<Button className="m-2 col-2" variant="outline-primary" onClick={getAllProducts}>Retrieve All Products</Button>
	</div>
	</div>	
	<Table id="bg-tableDB">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Price</th>
				<th>Availability</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			{
	    products.map((product, index) => (
	        <tr key={product._id}>
	            <td>{index + 1}</td>
	            <td>{product.title}</td>
	            <td>{product.description}</td>
	            <td>{product.price}</td>
	            <td>{product.isActive ? "Yes" : "No"}</td>
	           	<td>

	            <Button variant="outline-primary" onClick={() => updateProduct(product._id)}>Update</Button>
	            {/*<Button className="my-button" variant="outline-primary" as={Link} to={`/update-product/${product._id}`}>Update</Button>*/}
	            </td>
	            <td>
	           {product._id === selectedProductId && product.isActive !== true ? (
                               <Button variant="outline-success" onClick={() => handleReactivateProduct(product._id)}>Reactivate</Button>
                           ) :  ( 
                             <Button variant="outline-danger" onClick={() => handleDeactivateProduct(product._id)}>Deactivate</Button>
                           )}
                           </td>
	          				
	        </tr>
	    ))
			}
		</tbody>
	</Table>
</div>
	)
}
       