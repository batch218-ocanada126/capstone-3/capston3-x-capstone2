import Banner from '../components/Banner';
import ErrorLogo from '../images/error.webp';

// export default function Error(props) {



//   return (
//   	<>
  	
//   	 <Banner type="error" />
//   	</>
//   );
// }


    export default function Error(){
    
    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back to Product"
    }

    return (
        <>
    
         <img 
             src={ErrorLogo} 
             alt="TopLogo"
             style={{ width: '600px', height: '300px' }}
              ></img>
        
        <Banner data={data} />
       
     
        </>
    )

}