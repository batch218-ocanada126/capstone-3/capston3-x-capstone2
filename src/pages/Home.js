import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Footer from '../components/Footer';
import { useState } from 'react';




export default function Home() {

	

	const data = {
		title: "Crafty Ink",
		content: "Lorem Ipsum on lihtsalt proovitekst, mida kasutatakse printimis- ja ladumistööstuses. See on olnud tööstuse põhiline proovitekst juba alates 1500. aastatest, mil tundmatu printija võttis hulga suvalist teksti, et teha trükinäidist. Lorem Ipsum ei ole ainult viis sajandit säilinud, vaid on ka edasi kandunud elektroonilisse trükiladumisse, jäädes sealjuures peaaegu muutumatuks. See sai tuntuks 1960. aastatel Letraset'i lehtede väljalaskmisega, ja hiljuti tekstiredaktoritega nagu Aldus PageMaker, mis sisaldavad erinevaid Lorem Ipsumi versioone.",
		destination: "/products",
		label: "Buy Now!" 
	}
	return (
		<div id="homeBg" className="">
		{/* <Banner type="home" /> */}
		<Banner data={data} />
    	<Highlights />
    	<Footer />

		</div>
	)
}
