import {Button, Col, Row} from 'react-bootstrap';
import { Link } from "react-router-dom";


export default function Banner({data}){

    const {title, content, label} = data;

    return (

    <Row id="bannerBg" className="">
      
          <Col className="p-5">
           <h1 className="text-warning">{title}</h1>
           <p className="text-white">{content}</p>
       
           <Button variant="warning" as={Link} to={"/products"}>{label}</Button>
              
          </Col>  
    </Row>

)


}
