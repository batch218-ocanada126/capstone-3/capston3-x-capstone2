import {  useContext} from 'react';
import { Navigate } from "react-router-dom";

import {Link, NavLink} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { Form, Button} from "react-bootstrap";

import UserContext from '../UserContext';
import Stack from 'react-bootstrap/Stack';

import Logo from '../images/craftyink.png';




export default function AppNavbar() {

  // State to store user information upon user login.
  //const [user, setUser] = useState(localStorage.getItem('email'));

  // State to store user information upon login
  const { user } = useContext(UserContext);

  

  return (

    <Navbar id="bg-navbar" className="sticky-top container-fluid " expand="lg">
       
        
        <Navbar.Brand className=" col-5" as={Link} to="/"><img className="" src={Logo} style={{ width: '100px', height: '50px' }}></img> </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
       <Navbar.Collapse id="basic-navbar-nav">
        
           
             
             
          <Form id="searchBar" className=" col-md-8 col-sm-none d-flex">
                     
                      <Form.Control className=" "
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                      />
                      <Nav.Link as={NavLink} to="/products">
                      <Button variant="outline-warning" className="">Search</Button>
                      </Nav.Link>
                    </Form>

         
          <Nav className="">  
            
          
            {/*<Nav.Link as={NavLink} to="/products">Products</Nav.Link>*/}
           
            {
              (user.id !== null)  ?
            <Nav.Link as={NavLink} to="/logout" className="text-warning pl-5">Logout</Nav.Link>
            :
             <div className="d-flex m-2 "> {/*fragment is use if there are 2 or more return*/}

            <Nav.Link className="col-md-5 p-2 text-warning " as={NavLink} to="/login" >Login</Nav.Link>
            <Nav.Link className="col-md-5 p-2 text-warning" as={NavLink} to="/register">Register</Nav.Link>
             </div> }


            
            
            {
            (user.isAdmin === true)  ?
              
              
             <Nav.Link className="text-warning" as={NavLink} to="/adminDashboard">AdminDashboard</Nav.Link> 
             
              :
              <>
             
              <div className="d-none ">
              
               <Nav.Link as={NavLink} to="/adminDashboard">AdminDashboard</Nav.Link> 
              </div>
                </>
            }
          
         

          </Nav>
        </Navbar.Collapse>
    
    </Navbar>
  );
}