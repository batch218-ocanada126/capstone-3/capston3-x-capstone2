import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

import ProductQuantity from '../components/ProductQuantity';

import Swal from 'sweetalert2';

import { useParams, useNavigate, Link } from 'react-router-dom';


export default function ProductView(props) {

	//declaration that it will globally
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// 'useParams' - hook allows us to retrieve the productId passed via the URL params
	const {productId} = useParams();
	
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [totalAmount, setTotalAmount] = useState(0);
	const [product, setProduct] = useState(null);


	const handleQuantityChange = (newQuantity) => {
	setQuantity(newQuantity);
	setTotalAmount(price * newQuantity);

		}



	const checkOut = (productId) => {
	    const headers = {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${localStorage.getItem('token')}`
	    };
	    const body = JSON.stringify({
	        productId: productId,
	        quantity: quantity,
	        totalAmount: totalAmount
	    });

	   
	    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
	        method: "POST",
	        headers,
	        body
	    })
	        .then(res => res.json())
	        .then(data => {
	            console.log(data)
	            if (data !== true) {
	                Swal.fire({
	                    title: "Successfully Checkout",
	                    icon: "success",
	                    text: "You have successfully order for this product."
	                })
	                navigate("/products");
	            } else {
	                Swal.fire({
	                    title: "Something went wrong",
	                    icon: "error",
	                    text: "Please try again."
	                })
	            }
	        })
	      
	}



	 


	


	useEffect(() =>{

		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setTitle(data.title);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
			setTotalAmount(data.totalAmount);
		})

			
	}, [productId])


	return (

		<div className=" " >
			
				<Card className=" min-vh-100 " id="bg-productView" >
							
					      <Card.Body className="text-center row ">

					      		<div className="col-md-12 col-lg-6  p-5">
					        		<Card.Title className="text-dark"><h1>{title}</h1></Card.Title>
					       			
					       		</div>	

					       		<div id="descriptionBg" className="col-md-12 col-lg-6 p-5 border">	
					        		<Card.Subtitle className="text-white"><h3>Description:</h3></Card.Subtitle>
					        		        <Card.Text className="text-warning">{description}</Card.Text>
					        		        <Card.Text className="text-white">Price: ₱{price}</Card.Text>
					        		        <ProductQuantity 
					        		          handleQuantityChange={handleQuantityChange}
					        		          quantity={quantity}
					        		          setTotalAmount={setTotalAmount}
					        		        />
					        		        <Card.Text className="m-3 text-white">Total Amount: ₱{totalAmount}</Card.Text>

					        		        {
					        		        (user.id !== null) ?
					        		        <div>
					        		        	<Button onClick={() => checkOut(productId)}>Check Out</Button>
					        		        	<Link to="/products">
					        		       		<Button className="m-2 btn-yellow">Back to products</Button>
					        		       		</Link>
					        		       	</div>
					        		        :
					        				<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Buy</Button>   		 
					        		        }   		 
					        	</div>


					       		

					      </Card.Body>
					</Card>
			
			
		</div>

		)
	
}