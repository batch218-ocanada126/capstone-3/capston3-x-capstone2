

/*export default function ProductQuantity({initialQuantity, productId, price, setTotalAmount}) {
    const [quantity, setQuantity] = useState(initialQuantity);

    const handleQuantityChange = (newQuantity) => {
        setQuantity(newQuantity);
        setTotalAmount(price * newQuantity);
    }

    return (
        <div className="col-3">
            <input type="number" value={quantity} onChange={e => handleQuantityChange(e.target.value)}/>
        </div>
    )
}*/


import { useState } from 'react';

import React from 'react';

export default function ProductQuantity  ({ price, handleQuantityChange, totalAmount, setTotalAmount, quantity }) {


  return (
    <div>
      <label className="m-2 text-white">Quantity:</label>
      <input type="number" defaultValue={quantity} onChange={e => handleQuantityChange(e.target.value)} />
      {/*<p>Total Amount: ${totalAmount}</p>*/}

    </div>
  );
};