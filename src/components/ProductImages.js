import React from 'react';
import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';

import Tshirt1 from '../images/T-shirt.png';
import Tshirt2 from '../images/T-shirt2.png';
import Tshirt4 from '../images/T-shirtSinulog.png';

import Plate1 from '../images/Plate1.jpg';
import Plate3 from '../images/Plate3.jpg';
import Plate4 from '../images/Plate4.jpg';

import Mug1 from '../images/Mug.png';
import Mug2 from '../images/Mug2.png';
import Mug3 from '../images/Mug3.png';

import Keychain from '../images/Keychain.jpg';






export default function Images() {

	
	

	return (
		<>
		<h1 className="text-center m-5">OTHER PRODUCTS</h1>
		<CardGroup className="container-fluid shadow-lg mt-5">
		    <Card className="">
		      <Card.Img variant="top" src={Keychain} style={{ width: '425px', height: '200px' }} />
		      <Card.Body>
		        <Card.Title>Keychain</Card.Title>
		        <Card.Text>
		          näidist. Lorem Ipsum ei ole ainult viis sajandit säilinud, vaid on ka edasi kandunud elektroonilisse trükiladumisse, jäädes sealjuures peaaegu muutumatuks. See sai tuntuks 1960. aastatel Letraset'i lehtede väljalaskmisega, ja hiljuti tekstiredaktoritega nagu Aldus
		        </Card.Text>
		      </Card.Body>
		      <Card.Footer>
		        <small className="text-muted">Last updated 3 mins ago</small>
		      </Card.Footer>
		    </Card>
		    <Card className="">
		      <Card.Img variant="top" src="holder.js/100px160" />
		      <Card.Body>
		        <Card.Title>Card title</Card.Title>
		        <Card.Text>
		          This card has supporting text below as a natural lead-in to
		          additional content.{' '}
		        </Card.Text>
		      </Card.Body>
		      <Card.Footer>
		        <small className="text-muted">Last updated 3 mins ago</small>
		      </Card.Footer>
		    </Card>
		    <Card>
		      <Card.Img variant="top" src="holder.js/100px160" />
		      <Card.Body>
		        <Card.Title>Card title</Card.Title>
		        <Card.Text>
		          This is a wider card with supporting text below as a natural lead-in
		          to additional content. This card has even longer content than the
		          first to show that equal height action.
		        </Card.Text>
		      </Card.Body>
		      <Card.Footer>
		        <small className="text-muted">Last updated 3 mins ago</small>
		      </Card.Footer>
		    </Card>
		  </CardGroup>
		</>
		
	)
};