import { useState,  useContext } from "react";
import { Navigate } from "react-router-dom";
import { useParams } from 'react-router-dom';

import UserContext from "../UserContext";

import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";







export default function UpdateProductView() {

	
  		
  const { productId } = useParams();
 
  const [price, setPrice] = useState("");


 
  const { user } = useContext(UserContext);
 
 
  



  /*function updateProduct (e) {
  	 e.preventDefault();
  			console.log(productId)
          	    //Fetch the product data from the API
          	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
          	      .then(res => res.json())
          	      .then(data => {
          	        // set the product data in the state
          	        setProductData(data);
          	        setSelectedProductId(productId);
          	        // navigate to the update product view
   
          	      });
          	  

  	  const handleSubmit = () => {
  	    e.preventDefault();
  	    // Get the updated product data from the form inputs
  	    const updatedProductData = {
  	     
  	      price: productData.price.value
  	    }
 
  	     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PATCH',
			headers: {
			'Content-Type': 'application/json'
			},
				body: JSON.stringify({
				// title: productData.title,
				// description: productData.description,
				price: productData.price
				})
				})
			.then(res => res.json())
			.then(data => {
			console.log(data, 'Product updated');
			setProductData(data);
			 setSelectedProductId(productId);
			// navigate to the admin dashboard view
			})
			.catch(err => {
			console.log(err);
			});
			}
		}*/


  	function updateProduct(e) {
  	        
  	        e.preventDefault()
  	        
  	        console.log(productId)
  	        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
  	           method: "PATCH",
  	           headers: {
  	               'Content-Type' : 'application/json'
  	           },
  	           body: JSON.stringify({
  	            
  	            price: price
  	           })
  	        })
  	        .then(res=> res.json())
  	        .then(data => {
  	           console.log(data)

  	           if(data === true) {

  	               setPrice("");

  	               Swal.fire({
  	                       title: "Product Updated!",
  	                       icon: "success",
  	                       text: "Nice!"
  	                   }).then(()=>{window.location.reload();})

  	                
  	           } else {

  	               Swal.fire({
  	                   title: "Something went wrong",
  	                   icon: "error",
  	                   text: "Please, try again."
  	               }).then(()=>{window.location.reload();})
  	           }
  	        })
  	        
  	    }




  	/*  const handlePriceChange = (e) => {
  	    setProductData({...productData, price: e.target.value});
  	  }
        */
    

 


	return(

		(user.isAdmin === false || user.id === null) ?
		 <Navigate to ="/login" /> 
		 :
	
		<div className="container-fluid p-3 min-vh-100 " id="updateBg">
				<div className="">
		      <Form id="updateForm" className="border rounded col-3 p-3 m-5 text-center min-vh-50" onSubmit={(e) => updateProduct(e)}>
						<h3>Update Price</h3>
		        <Form.Label className="">
		          Price: 
		          <input className="" type="number" value={price} onChange={(e) => {setPrice(e.target.value)}}  />
		        </Form.Label>
		        <br />
		        <Button variant="outline-warning" type='submit '>Update Product</Button>
				</Form>
				</div>
		</div>
			
	)
}




