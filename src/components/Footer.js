

export default function Footer() {

	
	

	return (
		
	<div id="footer" className="main-footer mt-5 text-warning">
		<div className="container">
			<div className="row">
				<div className="col">
				<h5>CraftyInk PrintShop</h5>
				<div className="footer-text">
					<li>854-Bayabas st.</li>
					<li>Basak San Nicolas, Cebu City</li>
					<li>Philippines</li>
				</div>

		</div>
		<div className="col"><h5>Support</h5>
			<div className="footer-text">
				<li>Owner's Manuals</li>
				
				<li>Terms and Conditions</li>
			</div>
		</div>
		<div className="col"><h5>Brand</h5>
			<div className="footer-text">
				<li>CraftyInk</li>
			</div>
		</div>
		</div>
		<hr></hr>
		<div className="row">
		<p className="col-sm">©2023 CraftyInk PrintShop | All rights reserved | Terms Of Service | Privacy</p>
		</div>
	
		</div>

	</div>


		
		
	)
};



