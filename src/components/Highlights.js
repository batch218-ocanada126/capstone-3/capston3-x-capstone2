import {Row, Col , Card, Carousel} from 'react-bootstrap';
import Tshirt1 from '../images/T-shirt.png';
import Tshirt2 from '../images/T-shirt2.png';
import Tshirt4 from '../images/T-shirtSinulog.png';

import Plate1 from '../images/Plate1.jpg';
import Plate3 from '../images/Plate3.jpg';
import Plate4 from '../images/Plate4.jpg';

import Mug1 from '../images/Mug.png';
import Mug2 from '../images/Mug2.png';
import Mug3 from '../images/Mug3.png';





export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3  shadow">
	        <Col xs={12} md={4}>
	            <Carousel>
	                  <Carousel.Item className="border">
	                    <img
	                      className="d-block w-100"
	                      src={Tshirt1}
	                      alt="First slide"
	                    />
	                    <Carousel.Caption>
	                      <h3>First slide label</h3>
	                      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
	                    </Carousel.Caption>
	                  </Carousel.Item>
	                  <Carousel.Item>
	                    <img
	                      className="d-block w-100"
	                      src={Tshirt2}
	                      alt="Second slide"
	                    />

	                    <Carousel.Caption>
	                      <h3>Second slide label</h3>
	                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	                    </Carousel.Caption>
	                  </Carousel.Item>
	                  <Carousel.Item>
	                    <img
	                      className="d-block w-100"
	                      src={Tshirt4}
	                      alt="Third slide"
	                    />

	                    <Carousel.Caption>
	                      <h3>Third slide label</h3>
	                      <p>
	                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
	                      </p>
	                    </Carousel.Caption>
	                  </Carousel.Item>
	                </Carousel>
	        </Col>

	     {/*2nd Carousel*/}
	        <Col xs={12} md={4}>
	           <Carousel>
	           	                  <Carousel.Item className="border">
	           	                    <img
	           	                      className="d-block w-100"
	           	                      src={Plate1}
	           	                      alt="First slide"
	           	                    />
	           	                    <Carousel.Caption>
	           	                      <h3>First slide label</h3>
	           	                      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
	           	                    </Carousel.Caption>
	           	                  </Carousel.Item>
	           	                  <Carousel.Item>
	           	                    <img
	           	                      className="d-block w-100"
	           	                      src={Plate3}
	           	                      alt="Second slide"
	           	                    />

	           	                    <Carousel.Caption>
	           	                      <h3>Second slide label</h3>
	           	                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	           	                    </Carousel.Caption>
	           	                  </Carousel.Item>
	           	                  <Carousel.Item>
	           	                    <img
	           	                      className="d-block w-100"
	           	                      src={Plate4}
	           	                      alt="Third slide"
	           	                    />

	           	                    <Carousel.Caption>
	           	                      <h3>Third slide label</h3>
	           	                      <p>
	           	                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
	           	                      </p>
	           	                    </Carousel.Caption>
	           	                  </Carousel.Item>
	           	    </Carousel>
	        </Col>

	        {/*3rd Carousel*/}
	        <Col xs={12} md={4}>
	            <Carousel>
	           	                  <Carousel.Item className="border">
	           	                    <img
	           	                      className="d-block w-100"
	           	                      src={Mug1}
	           	                      alt="First slide"
	           	                    />
	           	                    <Carousel.Caption>
	           	                      <h3>First slide label</h3>
	           	                      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
	           	                    </Carousel.Caption>
	           	                  </Carousel.Item>
	           	                  <Carousel.Item>
	           	                    <img
	           	                      className="d-block w-100"
	           	                      src={Mug2}
	           	                      alt="Second slide"
	           	                    />

	           	                    <Carousel.Caption>
	           	                      <h3>Second slide label</h3>
	           	                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	           	                    </Carousel.Caption>
	           	                  </Carousel.Item>
	           	                  <Carousel.Item>
	           	                    <img
	           	                      className="d-block w-100"
	           	                      src={Mug3}
	           	                      alt="Third slide"
	           	                    />

	           	                    <Carousel.Caption>
	           	                      <h3>Third slide label</h3>
	           	                      <p>
	           	                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
	           	                      </p>
	           	                    </Carousel.Caption>
	           	                  </Carousel.Item>
	           	                </Carousel>
	        </Col>
	    </Row>
	)
}